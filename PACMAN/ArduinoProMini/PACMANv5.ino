// * Dust Sensor sampler
// *   Using a ATTiny85 to deal with the dust sensor signal
// *   Read on analog channel
// * Temperature using LM335A
// * Movement with PIR
// * Distance with Maxbotix EZ4
// * CO2 with Parallax unit
// * CO with Parallax unit
// * Timing controlled by AikoEvents library
// * Save using SdFat library with Adafruit uSD board
// * Analog deMux with 74HC4052
// * Time managed by Chronodot using Chronodot library by Stephanieplanet
// * In-temperature from Chronodot. Out-temperature from LM335A
//Add the SD Library
#include <SD.h>
//Add AikoEvents library
#include <AikoEvents.h>
using namespace Aiko;
//Add Libraries to work with the Real Time Clock
#include <Wire.h>
#include "Chronodot.h"  // Chronodot library by Stephanieplanet
Chronodot RTC;
DateTime time;
//Control AnalogDemux
int x9=6;
int x10=5;
//Dust sensor variable
float Dust;
//Temperature sensor variable
float Temp; //Temperature
//Distance variable
float Distance; //Distance
//CO2 variable
float CO2; //CO2 signal
//CO constants and variables
int COswpin=3; //CO switch pin
float CO; //CO signal
//variables for the CO sensor power cycle
unsigned long currTimeCO;
unsigned long prevTimeCO;
unsigned long currCOPwrTimer;
byte COPwrState,COstatus;
//Movement constants and variables
int PIRPin=4; //PIR input pin
int movement;
//General constants and variables
unsigned long rcount;
unsigned int SaveDeltaT=1000; //Save every X miliseconds
// Create the variables to be used by SdFat Library
File currfile;
// Savefile name in String and char format
String fname;
String currdir;
char file_fname[20];
char dir_dname[10];
long fcount,dcount;

float GetDust()
{
  unsigned long xRAW=0;
  selectDUST();
  pinMode(A0,INPUT);
  //Serial.println("readingDust");
  for (int ii=1;ii<=5;ii++){
//    xRAW=max(xRAW,analogRead(0));
//    xRAW=xRAW/50;
    xRAW=pulseIn(A0,HIGH)+xRAW;
  }
  delay(5);
  return (float) xRAW/5;
}
float GetDistance()
{
  unsigned int RAW=0;
  selectRF();
  for (int ii=1;ii<=50;ii++){
    RAW=RAW+analogRead(1);
  }
  return (float) RAW*(float) 100 / (float) 1024;
}
float GetTemperature()
{
  unsigned int RAW=0;
  selectTEMP();
  for (int ii=1;ii<=50;ii++){
    RAW=RAW+analogRead(0);
  }
  return (float) RAW*(float) 100 / (float) 1024;
}
float GetCO2()
{
  unsigned int RAW=0;
  selectCO2();
  for (int ii=1;ii<=50;ii++){
    RAW=RAW+analogRead(0);
  }
  return (float) RAW*(float) 100 / (float) 1024;
}
float GetCO()
{
  unsigned int RAW=0;
  selectCO();
  for (int ii=1;ii<=50;ii++){
    RAW=RAW+analogRead(0);
  }
  return (float) RAW*(float) 100 / (float) 1024;
}
void COPwrCycler(){
    currTimeCO = millis();
    if (currTimeCO - prevTimeCO > currCOPwrTimer){
      prevTimeCO = currTimeCO;
      COstatus=1;
    if(COPwrState == 71){
      COPwrState = 255;
      currCOPwrTimer = 60000;  //60 seconds at 5v
    }
    else{
      COPwrState = 71;
      currCOPwrTimer = 90000;  //90 seconds at 1.4v
    }
    analogWrite(COswpin,COPwrState);
    if (COPwrState==71){
      COstatus=COstatus+1;
    }
    else{
      COstatus=1;
    }
  }
}

String timestring()
{
  time=RTC.now();
  String xx=String(String(time.year())+"\t"+String(time.month())+"\t"+String(time.day()));
  String xx2=String(xx+"\t"+String(time.hour())+"\t"+String(time.minute())+"\t"+String(time.second()));
  return xx2;
}
void checkCMD(){
  if (Serial.available() > 0){
    if (Serial.read()=='Q'){
    //If the chatacter Q was received through the serial port, then stop recording
      downloadData();
    }
  }
  else {
    return;
  }
}
void SaveData()
{
  //Movement from PIR
  if (digitalRead(PIRPin)==LOW){movement=1;}
  //Dust from Dust sensor
  Dust=GetDust();
  //Distance from Range Finder
  Distance=GetDistance();
  //Serial.println("Distance Done");
  //Temperature from analog
  Temp=GetTemperature()/10-273.15;
  //Serial.println("Temperature Done");
  //CO2 from analog
  CO2=GetCO2();
  //Serial.println("CO2 Done");
  //CO from analog
  CO=GetCO();
  //Serial.println("CO Done");
  //Dust from Dust sensor
//  Dust=GetDust();
  //Serial.println("Dust Done");
  //Increment Record count
  rcount++;
  //Get the current time string
  String currTstr=timestring();
  //Serial.println("Current Time Done");
  //Output the measured values
  //To the serial port
  Serial.print(rcount);
  Serial.print("\t");
  Serial.print(currTstr);
  Serial.print("\t");
  Serial.print(Distance);
  Serial.print("\t");
  Serial.print(Temp);
  Serial.print("\t");
  Serial.print(time.tempC());
  Serial.print("\t");
  Serial.print(Dust);
  Serial.print("\t");
  Serial.print(CO2);
  Serial.print("\t");
  Serial.print(CO);
  Serial.print("\t");
  Serial.print(movement);
  Serial.print("\t");
  Serial.println(COstatus);
  //To the file output
  //Open the current file
  //Serial.println("Opening file");
  //digitalWrite(13,HIGH);
  fname.toCharArray(file_fname,fname.length()+1);
  currfile=SD.open(file_fname, FILE_WRITE);
  //Write to the file
  //Serial.println("Writing to file");
  currfile.print(rcount);
  currfile.print("\t");
  currfile.print(currTstr);
  currfile.print("\t");
  currfile.print(Distance);
  currfile.print(" \t");
  currfile.print(Temp);
  currfile.print("\t");
  currfile.print(time.tempC());
  currfile.print("\t");
  currfile.print(Dust);
  currfile.print("\t");
  currfile.print(CO2);
  currfile.print("\t");
  currfile.print(CO);
  currfile.print("\t");
  currfile.print(movement);
  currfile.print("\t");
  currfile.println(COstatus);
  currfile.close();
  //Serial.println("Writing to file Done");
  if (rcount>=36000){
    rcount=0;
    fcount++;
    fname=String("PAM_"+String(fcount)+".txt");
    fname.toCharArray(file_fname,fname.length()+1);
    currfile=SD.open(file_fname,FILE_WRITE);
    currfile.println("Count\tYear\tMonth\tDay\tHour\tMinute\tSecond\tDistance\tTemperature_mV\tTemperature_IN_C\tPM_mV\tCO2_mV\tCO_mV\tMovement\tCOstatus");
    currfile.close();
  }
  //Reinitialise the variables
  Dust=0.0;
  Distance=0.0;
  Temp=0.0;
  CO2=0.0;
  CO=0.0;
  movement=0;
  //check CO pwr cycle
  COPwrCycler();
  //pat the watchdog
  //wdt_reset();
}
void downloadData(){
  //disable watchdog
  //wdt_disable();
  //Stop events
  Events.reset();
  byte incm;
  //wait for the OK to send
  do {
    incm=Serial.read();
    delay(500);
  } while (incm!='S');
  //send all the data in the current folder
  for (long xx=0;xx<=fcount;xx++){
    fname=String("PAM_"+String(xx)+".txt");
    fname.toCharArray(file_fname,fname.length()+1);
    currfile=SD.open(file_fname,FILE_WRITE);
    if (currfile){
      while (currfile.available()){
        Serial.write(currfile.read());
      }
      currfile.close();
    }
    else{
      Serial.println("No more files?");
    }
  }
  //move folder count and reset filecount
  dcount++;
  currdir=String(dcount);
  currdir.toCharArray(dir_dname,currdir.length()+1);
  SD.mkdir(dir_dname);
  fcount=0;
  //wait for the OK to resume
  do {
    incm=Serial.read();
    delay(500);
  } while (incm!='R');
  //Register the events
  Events.addHandler(SaveData,SaveDeltaT);
  Events.addHandler(checkCMD,SaveDeltaT);
  //enable watchdog
  //wdt_enable(WDTO_4S); //4 seconds watchdog
}
void selectDUST(){
  digitalWrite(x9,LOW);
  digitalWrite(x10,LOW);
}
void selectRF(){
  digitalWrite(x9,LOW);
  digitalWrite(x10,LOW);
}
void selectTEMP(){
  digitalWrite(x9,LOW);
  digitalWrite(x10,HIGH);
}
void selectCO2(){
  digitalWrite(x9,HIGH);
  digitalWrite(x10,LOW);
}
void selectCO(){
  digitalWrite(x9,HIGH);
  digitalWrite(x10,HIGH);
}
void setup(){
  //Set up serial comms
  Serial.begin(57600);
  //Set up RTC
  Wire.begin();
  RTC.begin();   // the function to get the time from the RTC
  time=RTC.now();
  String currTstr=timestring();
  Serial.println(currTstr);
  //Set up PIR
  Serial.println("Setting PIR");
  pinMode(PIRPin,INPUT);
  //Set up AnalogDemux
  Serial.println("Setting DeMux");
  pinMode(x9,OUTPUT);
  pinMode(x10,OUTPUT);
  //Set up file(s)
  fcount=0; //File index
  dcount=0; //Directory index
  Serial.println("Initialising SD card");
  pinMode(10, OUTPUT); //Pin 10 must be set as an output for the SD communication to work.
  if (!SD.begin(10)){
    //Initialize the SD card and configure the I/O pins.
    Serial.println("Initialisation failed!!");
    return;
  }
  Serial.println("Initialisation done.");
  currdir=String(dcount);
  currdir.toCharArray(dir_dname,currdir.length()+1);
  SD.mkdir(dir_dname);
  fname=String(currdir+"/PAM_"+String(fcount)+".txt");
  fname.toCharArray(file_fname,fname.length()+1);
  while (SD.exists(file_fname)){
    fcount++; //Creates a new folder
    currdir=String(dcount);
    currdir.toCharArray(dir_dname,currdir.length()+1);
    SD.mkdir(dir_dname);
    fname=String(currdir+"/PAM_"+String(fcount)+".txt");
    fname.toCharArray(file_fname,fname.length()+1);
    Serial.println("Trying folder/file");
    Serial.println(file_fname);
  }
  // Check that file exist and move COUNT until you hit a new file
  currfile=SD.open(file_fname, FILE_WRITE);
  currfile.println("Count\tYear\tMonth\tDay\tHour\tMinute\tSecond\tDistance\tTemperature_mV\tTemperature_IN_C\tPM_mV\tCO2_mV\tCO_mV\tMovement\tCOstatus");
  currfile.close(); //Close the file
  Serial.println(file_fname);
  //Serial data headers
  Serial.println("Count\tYear\tMonth\tDay\tHour\tMinute\tSecond\tDistance\tTemperature_mV\tTemperature_IN_C\tPM_mV\tCO2_mV\tCO_mV\tMovement\tCOstatus");
  //Initialise variables
  Dust=0.0;
  Distance=0.0;
  Temp=0.0;
  movement=0;
  CO2=0.0;
  CO=0.0;
  //Initialise CO cycling
  currTimeCO=0;
  prevTimeCO=0;
  currCOPwrTimer=500;
  COPwrState=71;
  COstatus=1;
  pinMode(COswpin,OUTPUT);
  //Register the events
  Events.addHandler(SaveData,SaveDeltaT);
  Events.addHandler(checkCMD,SaveDeltaT);
}
void loop(){
  Events.loop();
}
