//* Dust sensor reader
// *   Based on "Dusty's" code for SHARP's dust sensor
// *   Timing constants optimised (manually) for use with\
//     an ATtiny85 @8MHz with internal clock
//Dust Sensor constants and variables
int dustIN=1;  //Analog input Pin1 on ATtiny85
float dust;  //Dust voltage
int dustTRIG=3;  //Trigger pin for Dust sensor 
int delay_tosample=150; //Microseconds after the trigger
int delay2=1; //other timer to complete the 320us pulse
int dustOUT=0; //PWM out for analog out of averaged signal
int Nsamples=50;
void setup(){
  pinMode(dustTRIG,OUTPUT);
  pinMode(dustOUT,OUTPUT);
  pinMode(4,OUTPUT);
}
void loop(){
  digitalWrite(4,HIGH);
  digitalWrite(dustTRIG,LOW);
  delayMicroseconds(delay_tosample);
  dust=analogRead(dustIN);
  delayMicroseconds(delay2);
  digitalWrite(dustTRIG,HIGH);
  analogWrite(dustOUT,dust/4);
//  digitalWrite(4,HIGH);
//  for (long int i=0;i<=10000;i++){}
  delay(10);
  digitalWrite(4,LOW);
}
